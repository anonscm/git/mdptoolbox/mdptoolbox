function T = make_tree(nodes, I)

% tree  Return the children of a set of nodes 
% and the development of the children
% Arguments ---------------------------------------------------------------
% nodes: set of nodes of a same level (ie related to the same variable) to develop
%        (matrix (1xc_last_child) 
% I (cell array) : influence diagram 
% Evaluation --------------------------------------------------------------
% T: set of all children nodes 
%--------------------------------------------------------------------------

%% Initialization
% Define columns of the tree matrix
set_tree_constants();

%% find children nodes using recursion
if isempty(nodes)
    % define the root node
    v = 1; % start with the first variable
    if I.Ntype(v) == 3 % utility variable
        T = [];
    else
        k = I.Nval(v); % domain size of v is the number of children
        n = 1; % first declared node
        T = zeros(n, c_last_child);
        T(n, c_index) = n;
        T(n, c_var) = v; 
        T(n, c_p) = -1;
        T(n, c_first_child:c_first_child+k-1) = n+1:n+k;
        T = [T ; make_tree(T, I)];
    end
else
    q = size(nodes,1); % number of node in nodes
    v = nodes(1,c_var); % variable related to each node of nodes
    k = I.Nval(v); % domain size of v is the number of children
    vnext = v+1; % next variable in the list
    knext = I.Nval(vnext); % Nval is zero for utility variable
    T = zeros(q*k, c_last_child); 
    n = max(nodes(:,c_index)); % find the last number of declared node 
    N = max(max(nodes(:,c_first_child:c_last_child))); % find the last number of evocated children node 
    for i = 1:q % for each node of nodes
        for j = 1:k % for each value of the related variable of nodes in nodes
            r = (i-1)*k+j; 
            T(r,c_index) = n+(i-1)*k+j;
            T(r,c_parent_var) = v;
            T(r,c_parent_value) = j;
            T(r,c_parent_index) = nodes(i,c_index);
            T(r,c_var) = vnext;
            T(r, c_p) = -1;
            s = N+(i-1)*k*knext+(j-1)*knext;
            T(r,c_first_child:c_first_child+knext-1) = (s+1):(s+knext);
        end
    end
    if I.Ntype(vnext) ~= 3 % Stop recursion when utility variables encountered (no child)
        T = [T ; make_tree(T,I)];
    end
end 
  
