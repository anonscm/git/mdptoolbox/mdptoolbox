function TEST()

%% Define tests
DIs = {id_example_entrepreneur(), id_example_wear(), id_example_student(), ...
            test1(), test2(), test3(), test4(), test5() , test6(), test7()};
labels = { 'id_example_entrepreneur', 'id_example_wear()', 'id_example_student', ...
            'test1', 'test2', 'test3', 'test4', 'test5' , 'test6', 'test7'};
MEUs = zeros(1,length(DIs));
for i= 1:length(DIs)
    POLICYs{i} = cell(length(DIs{i}.Nval), 1);
end

%% Expected results
% id_example_entrepreneur
MEUs(1) = 2.25;
p = POLICYs{1}; 
p{1} = 2;
p{3} = [1     1     1     2; ...
        1     2     2     1];
POLICYs{1} = p;

% id_example_wear
MEUs(2) = 32.16;
p = POLICYs{2}; 
p{1} = 2;
POLICYs{2} = p;

% id_example_student
MEUs(3) = 55;
p = POLICYs{3};
p{3} = ones(2);
p{6} = ones([2 2 2 2 2]);
POLICYs{3} = p;

% TEST1
MEUs(4) = 23;
p = POLICYs{4};
p{1} = 1;
POLICYs{4} = p;

% TEST2
MEUs(5) = 11.4;
p = POLICYs{5};
p{1} = 2;
POLICYs{5} = p;

% TEST3
MEUs(6) = 11.5;

% TEST4
MEUs(7) = 15;
p = POLICYs{7};
p{1}=2;
p{2}=[1 ; 1];
POLICYs{7} = p;

% TEST5
MEUs(9) = 9;
p = POLICYs{8};
p{2}=[1 ; 2]; 
POLICYs{8} = p;

% TEST6
MEUs(9) = 68;
p = POLICYs{9};
p{2}=[2 ; 2]; 
p{3}=[2 1;1 2]; 
POLICYs{9} = p;

% TEST7
MEUs(10) = 13;
p = POLICYs{10};
p{2}=[1 ; 1]; 
p{4}=2*ones([2 2 2]); 
POLICYs{10} = p;


%% Launch tests       
precision_meu = 0.001;
for i=1:length(DIs)
    [policyBI, meuBI, cpu_time] = id_backward_induction(DIs{i});
    [policyVE, meuVE, cpu_time] = id_variable_elimination(DIs{i});
   
    is_OK = true;
    error_msg = '';
    if abs(meuBI - MEUs(i)) > precision_meu || abs(meuVE - MEUs(i)) > precision_meu 
         error_msg = strcat(error_msg, ' MEU error ');
    end
    p = POLICYs{i};
    for j=1:length(DIs{i}.Nval)
         if ~isempty(p{j})
             if size(policyBI{j}) == size(p{j}) & size(policyVE{j}) == size(p{j})
                 if ~all(policyBI{j} == p{j}) | ~all(policyVE{j} == p{j})
                      error_msg = strcat(error_msg, ' Policy error ');
                 end
            end
        end
    end
    if is_OK
        display ([labels{i} ': succeed']);
    else
        display ([labels{i} ': failed' error_msg]);
    end

end




%% TEST1
function I = test1()
% Variables type, 1: nature, 2: decision, 3: utility
I.Ntype = [2 3 3]; 
N = length(I.Ntype); % number of variables

% Variables label
I.Nlabel = cell(1,N);
I.Nlabel{1} = 'D';
I.Nlabel{2} = 'V1';
I.Nlabel{3} = 'V2';

% Domain size
I.Nval = [2 0 0];

% Variable relation: DAG
D = 1; V1 = 2; V2 = 3; 
I.G = zeros(N,N);
I.G(D, [V1 V2]) = 1;

% Conditional probabilities distributions
I.CPD = cell(1,N);
% the vector is ranged varying V1 and at last Vn
% For example for variables V1, V2 influencibg V3 with 2 values: 1,2
% V1 V2 V3
% 1 1 1 
% 2 1 1
% 1 2 1
% 2 2 1
% 1 1 2
% 2 1 2
% 1 2 2
% 2 2 2

% Utilities
I.V = cell(1,N);
I.V{2} = [10; 5];
I.V{3} = [13; 4];



%% TEST2
function I = test2()
% Variables type, 1: nature, 2: decision, 3: utility
I.Ntype = [2 1 3]; 
N = size(I.Ntype,2); % number of variables

% Variables label
I.Nlabel = cell(1,N);
I.Nlabel{1} = 'D';
I.Nlabel{2} = 'X1';
I.Nlabel{3} = 'V1';

% Domain size
I.Nval = [2 2 0];

% Variable relation: DAG
D = 1; X1 = 2; V1 = 3;
G = zeros(N,N);
I.G(D, V1) = 1;
I.G(X1, V1) = 1;

% Conditional probabilities distributions
I.CPD = cell(1,N);
I.CPD{2} = [0.7; 0.3];

% Utilities
I.V = cell(1,N);
I.V{3} = [10 7;15 3];


%% TEST3
function I = test3()
% Variables type, 1: nature, 2: decision, 3: utility
I.Ntype = [1 3]; 
N = size(I.Ntype,2); % number of variables

% Variables label
I.Nlabel = cell(1,N);
I.Nlabel{1} = 'X1';
I.Nlabel{2} = 'V1';

% Domain size
I.Nval = [2 0];

% Variable relation: DAG
X1 = 1; V1 = 2;
I.G = zeros(N,N);
I.G(X1, V1) = 1;

% Conditional probabilities distributions
I.CPD = cell(1,N);
I.CPD{1} = [0.7; 0.3];

% Utilities
I.V = cell(1,N);
I.V{2} = [10;15];


%% TEST4
function I = test4()
% Variables type, 1: nature, 2: decision, 3: utility
I.Ntype = [2 2 3]; 
N = size(I.Ntype,2); % number of variables

% Variables label
I.Nlabel = cell(1,N);
I.Nlabel{1} = 'D1';
I.Nlabel{2} = 'D2';
I.Nlabel{3} = 'V1';

% Domain size
I.Nval = [2 2 0];

% Variable relation: DAG
D1 = 1; D2 = 2; V1 = 3;
I.G = zeros(N,N);
I.G(D1, V1) = 1;
I.G(D2, V1) = 1;

% Conditional probabilities distributions
I.CPD = cell(1,N);

% Utilities
I.V = cell(1,N);
I.V{3} = [10 9; 15 13];


%% TEST5
function I = test5()
% Variables type, 1: nature, 2: decision, 3: utility
I.Ntype = [1 2 3]; 
N = size(I.Ntype,2); % number of variables

% Variables label
I.Nlabel = cell(1,N);
I.Nlabel{1} = 'X1';
I.Nlabel{2} = 'D';
I.Nlabel{3} = 'V1';

% Domain size
I.Nval = [2 2 0];

% Variable relation: DAG
X1 = 1; D = 2; V1 = 3;
I.G = zeros(N,N);
I.G(D, V1) = 1;
I.G(X1, V1) = 1;
I.G(X1, D) = 1;

% Conditional probabilities distributions
I.CPD = cell(1,N);
I.CPD{1} = [0.5;0.5];

% Utilities
I.V = cell(1,N);
I.V{3} = [10 3;5 8];


%% TEST6
function I = test6()
% Variables type, 1: nature, 2: decision, 3: utility
I.Ntype = [1 2 2 3]; 
N = size(I.Ntype,2); % number of variables

% Variables label
I.Nlabel = cell(1,N);
I.Nlabel{1} = 'X1';
I.Nlabel{2} = 'D1';
I.Nlabel{3} = 'D2';
I.Nlabel{4} = 'V1';

% Domain size
I.Nval = [2 2 2 0];

% Variable relation: DAG
I.G = zeros(N,N);
I.G(1, 2) = 1;
I.G(2, 3) = 1;
I.G(3, 4) = 1;
I.G(2,4) = 1;
I.G(1, 4) = 1;

% Conditional probabilities distributions
I.CPD = cell(1,N);
I.CPD{1} = [0.5;0.5];

% Utilities
I.V = cell(1,N);
I.V{4} = reshape([10 15 58 2 24 6 58 78], [2 2 2]);


%% TEST7
function I = test7()
% Variables type, 1: nature, 2: decision, 3: utility
I.Ntype = [1 2 1 2 3]; 
N = size(I.Ntype,2); % number of variables

% Variables label
I.Nlabel = cell(1,N);
I.Nlabel{1} = 'X1';
I.Nlabel{2} = 'D1';
I.Nlabel{3} = 'X2';
I.Nlabel{4} = 'D2';
I.Nlabel{5} = 'V';

% Domain size
I.Nval = [2 2 2 2 0];

% Variable relation: DAG
I.G = zeros(N,N);
I.G(1, 2) = 1;
I.G(2, [3 4]) = 1;
I.G(3, 4) = 1;
I.G(4, 5) = 1;

% Conditional probabilities distributions
I.CPD = cell(1,N);
I.CPD{1} = [0.5;0.5];
I.CPD{3} = reshape([0.3 0.7 0.4 0.6], [2 2]);

%Utilities
I.V = cell(1,N);
I.V{5} = [12; 13];


