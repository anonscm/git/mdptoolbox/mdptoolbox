function I = id_example_wear()


% Luque 2008.

% Variables type, 1: nature, 2: decision, 3: utility
Ntype = [2 1 1 1 3 3]; 
N = size(Ntype,2); % number of variables

% Variables label
Nlabel = cell(1,N);
Nlabel{1} = 'D';
Nlabel{2} = 'X1';
Nlabel{3} = 'X2';
Nlabel{4} = 'X3';
Nlabel{5} = 'U1';
Nlabel{6} = 'U2';

% Domain size
Nval = [2 2 2 2 0 0];

% Variable relation: DAG
D = 1; X1 = 2; X2 = 3; X3 = 4; U1 = 5; U2 = 6; 
G = zeros(N,N);
G(D, [U1 U2]) = 1;
G(X1, X2) = 1;
G(X2, U1) = 1;
G(X3, U2) = 1;

% Conditional probabilities distributions
CPD = cell(1,N);
% the vector is ranged varying V1 and at last Vn
% For example for variables V1, V2 influencibg V3 with 2 values: 1,2
% V1 V2 V3
% 1 1 1 
% 2 1 1
% 1 2 1
% 2 2 1
% 1 1 2
% 2 1 2
% 1 2 2
% 2 2 2
CPD{2} = reshape([.8 .2],[2 1]);
CPD{3} = reshape([.9 .05 .1 .95],[2 2]);
CPD{4} = reshape([.8 .2],[2 1]);

% Utilities
V = cell(1,N);
V{5} = reshape([35 42 40 30],[2 2]);
V{6} = reshape([-8 -6 -6 -9],[2 2]);

% Influence diagram
I.G = G;
I.Ntype = Ntype;
I.Nlabel = Nlabel;
I.Nval = Nval;
I.CPD = CPD;
I.V= V;
