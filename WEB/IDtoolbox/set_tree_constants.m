%% define index of column of the matrix representing the tree
c_index = 1;
c_parent_var = 2;
c_parent_value = 3;
c_parent_index = 4;
c_var = 5;
c_p = 6;
c_meu = 7;
c_first_child = 8;
c_last_child = c_first_child + max(I.Nval) -1; 
