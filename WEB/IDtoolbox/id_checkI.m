function is_OK = id_checkI(I)

is_OK = true;

%% test consistent number of variables
n = length(I.Ntype);
if size(I.G) ~= [n n]
    display 'ERROR: G at least one dimension of G is not consistent with Ntype dimension.'; is_OK = true;
elseif length(I.Nlabel)~= n
    display 'ERROR: I.Nlabel dimension is not consistent with Ntype dimension.'; is_OK = true;
elseif length(I.Nval)~= n
    display 'ERROR: I.Nval dimension is not consistent with Ntype dimension.'; is_OK = true;
elseif length(I.CPD)~= n
    display 'ERROR: I.CPD dimension is not consistent with Ntype dimension.'; is_OK = true;
elseif length(I.V)~= n
    display 'ERROR: I.V dimension is not consistent with Ntype dimension.'; is_OK = true;
end

%% test values
if any(any(I.G~=0 & I.G~=1))
    display 'ERROR: I.G contains element different of 0 and 1.'; is_OK = true;
end
if any(any(I.Ntype~=1 & I.Ntype~=2 & I.Ntype~=3))
    display 'ERROR: I.Ntype contains element different of 1, 2 and 3.'; is_OK = true;
end
if any(I.Nval(I.Ntype==3))
    display 'ERROR: I.Nval must be zero for utility variables.'; is_OK = true;
end

for i=1:length(I.CPD)
    if I.Ntype(i) == 1 % nature variable
        V = reshape(I.CPD{i}, prod(size(I.CPD{i})), 1);
        if any(V<0 | V>1)
            display (['ERROR: I.CPD{' num2str(i) '} contains probabilities, must be in [0 1].']); is_OK = true;
        end
    elseif ~isempty(I.CPD{i})
        display (['ERROR: I.CPD{' num2str(i) '} non empty for a non nature variable.']); is_OK = true;
    end
end

%% test order of variable
% test that all utility variables are at the end
if find(I.Ntype == 3,1,'first') < find(I.Ntype == 1 | I.Ntype == 2,1,'last')
    display 'ERROR: All utility variables must be at the end in I.Nval.'; is_OK = true;
end

% test order of nature and decision variables
% TODO   