function I = id_example_entrepreneur()

% id_example_entrepreneur Generate an example influence diagram
% For more detail, see Probabilistic Graphical Models. 
% D. Koller and N. Friedman. MIT Press 2009.

% Variables type, 1: nature, 2: decision, 3: utility
Ntype = [2 1 2 1 3 3]; 
N = size(Ntype,2); % number of variables

% Variables label
Nlabel = cell(1,N);
Nlabel{1} = 'C';
Nlabel{2} = 'S';
Nlabel{3} = 'F';
Nlabel{4} = 'M';
Nlabel{5} = 'W';
Nlabel{6} = 'U';

% Domain size
Nval = [2 4 2 3 0 0];

% Variable relation: DAG
C = 1; S = 2; F = 3; M = 4; W = 5; U = 6; 
G = zeros(N,N);
G(C, [S W]) = 1;
G(S, F) = 1;
G(M, [S U]) = 1;
G(F, U) = 1;

% Conditional probabilities distributions
CPD = cell(1,N);
% the vector is ranged varying V1 and at last Vn
% For example for variables V1, V2 influencibg V3 with 2 values: 1,2
% V1 V2 V3
% 1 1 1 
% 2 1 1
% 1 2 1
% 2 2 1
% 1 1 2
% 2 1 2
% 1 2 2
% 2 2 2
CPD{2} = reshape([0 0.6 0 0.3 0 0.1 0 0.3 0 0.4 0 0.4 0 0.1 0 0.3 0 0.5 1 0 1 0 1 0],[2 3 4]);
CPD{4} = reshape([0.5 0.3 0.2],[3 1]);

% Utilities
V = cell(1,N);
V{5} = reshape([0 -1],[2 1]);
V{6} = reshape([0 -7 0 5 0 20],[2 3]);

% Influence diagram
I.G = G;
I.Ntype = Ntype;
I.Nlabel = Nlabel;
I.Nval = Nval;
I.CPD = CPD;
I.V= V;

