%% id_variable_elimination  Resolution of influence diagram with
%                           variable elimination algorithm
% Arguments ---------------------------------------------------------------
% Let N be the number of variable
% I (cell array) : influence diagram
%   I.G (NxN) : adjacence graph (variables relation)
%   I.Ntype (N) : variable type
%   I.Nlabel (N) : variable label
%   I.Nval (N) : variable domain size
%   I.CPD (cell(N)) : conditional probability distribution
%   I.V (cell(N)) : utility function
% Evaluation --------------------------------------------------------------
% Let n be the number of decision variable
%   policy(cell(N)) : optimal policy
%   meu         : optimal expected utility
%   cpu_time    : used CPU time
%--------------------------------------------------------------------------
function [policy,meu,cpu_time] = id_variable_elimination(I)

    % We assume that I has been "id_reorder" before
    cpu_time = cputime;
    N = length(I.Ntype); % number of variables


    %% Initialisation of Phi and Mu using the CPD and utility tables given into the ID (I)
    set_phi = {};
    set_mu = {};

    % For each variable
    for w=1:N
        if I.Ntype(w) == 1 % nature variable
            % put w's parents in the ID and w into the scope,
            % and sort the scope (needed because w is not necessary at the end)
            [phi.scope,order] = sort([find((I.G(:,w))' == 1),w]);
            phi.table = I.CPD{w};
            if length(order) > 1
                phi.table = permute(phi.table,order); % reordering the dimensions
            end
            phi = normalize(phi);
            set_phi = [set_phi, phi];
        elseif I.Ntype(w) == 3 % utility variable
            mu.scope = find((I.G(:,w))' == 1);
            mu.table = I.V{w};
            mu = normalize(mu);
            set_mu = [set_mu, mu];
        end
    end

    %% For each variable to be eliminated in the reverse strong ordering
    policy = cell(1,N);

    for w=N:-1:1

        % We don't need to eliminate utility variables (type = 3)
        if I.Ntype(w) ~= 3
            set_phi_w = {};
            set_mu_w = {};

            % Filling Phi_w
            id_to_delete = [];
            for j=1:length(set_phi)
                if any(set_phi{j}.scope == w)
                    id_to_delete = [id_to_delete j];
                end
            end
            set_phi_w = set_phi(id_to_delete);
            set_phi(id_to_delete) = [];

            % Filling Mu_w
            id_to_delete = [];
            for j=1:length(set_mu)
                if any(set_mu{j}.scope == w)
                    id_to_delete = [id_to_delete j];
                end
            end
            set_mu_w = set_mu(id_to_delete);
            set_mu(id_to_delete) = [];


            % Marginalization
            marg_type = 1; % decision variable
            if I.Ntype(w) == 1 % nature variable
                marg_type = 0;
            end
            [phi_agr, mu_agr, delta_w] = marginalize(set_phi_w, set_mu_w, w, I.Nval, marg_type);

            % Updating Phi and Mu
            set_phi = [set_phi phi_agr];
            set_mu = [set_mu op_factors(mu_agr,phi_agr,I.Nval,2)];

            policy{w} = delta_w;
        end

    end
    
    %% Compute extended policy
    policy_ext = cell(1,N);
    
    for w=1:N       
        if ~isempty(policy{w})    
            % Scope
            new_scope = [];
            old_scope = policy{w}.scope;
            if w > 1
                new_scope = 1:(w-1);
            end
            policy_ext{w}.scope = new_scope;
            
            % Table
            if isempty(old_scope) && isempty(new_scope)
                policy_ext{w}.table = policy{w}.table;
            elseif isempty(old_scope) && ~isempty(new_scope)
                if length(new_scope) == 1
                    policy_ext{w}.table = repmat(policy{w}.table, [I.Nval(new_scope) 1]);
                else
                    policy_ext{w}.table = repmat(policy{w}.table, I.Nval(new_scope));
                end
            else % ~isempty() && ~isempty
                scope_ext = setdiff(new_scope, old_scope);               
                new_table_unsorted = repmat(policy{w}.table, [ones(1,length(old_scope)) I.Nval(scope_ext)]);
                [~,B] = sort([old_scope scope_ext]);
                if length(B) > 1
                    policy_ext{w}.table = permute(new_table_unsorted, B);
                else
                    policy_ext{w}.table = new_table_unsorted;
                end
            end
        end
    end
    %%
    
    policy = cell(1,N);
    % We don't need scopes
    for w=1:N
       if ~isempty(policy_ext{w})
           policy{w} = policy_ext{w}.table;
       end
    end
    
    meu = set_mu{1}.table;
    cpu_time = cputime - cpu_time;
    
end

%% normalize - transform row array into col array if necessary
function [factor_norm] = normalize(factor)

    factor_norm.scope = factor.scope;

    if length(factor.scope) == 1 && size(factor.table,1) == 1
        factor_norm.table = factor.table.';
    else
        factor_norm.table = factor.table;
    end
    
end

%% op_factors - operation over two factors
% op = 0 : f1+f2
% op = 1 : f1*f2
% op = 2 : f1/f2
function [f] = op_factors(f1, f2, Nval, op)

    %% The two scopes are empty [OK]
    if isempty(f1.scope) && isempty(f2.scope)
        f.scope = [];
        if op == 0
            f.table = f1.table + f2.table;
        elseif op == 1
            f.table = f1.table .* f2.table;
        elseif op == 2
            f.table = f1.table ./ f2.table;
        else
            error('ERROR: this operation does not exist');
        end
        %% If f1.scope is empty [OK]
    elseif isempty(f1.scope)
        f.scope = f2.scope;
        if op == 0
            f.table = f2.table + f1.table(1);
        elseif op == 1
            f.table = f2.table .* f1.table(1);
        elseif op == 2
            f.table = f1.table(1) .* (1 ./ f2.table);
        else
            error('ERROR: this operation does not exist');
        end
        %% If f2.scope is empty [OK]
    elseif isempty(f2.scope)
        f.scope = f1.scope;
        if op == 0
            f.table = f1.table + f2.table(1);
        elseif op == 1
            f.table = f1.table .* f2.table(1);
        elseif op == 2
            f.table = f1.table ./ f2.table(1);
        else
            error('ERROR: this operation does not exist');
        end
        %% The two scopes aren't empty
    else
        table1 = squeeze(repmat(f1.table, [ones(1,length(f1.scope)) Nval(setdiff(f2.scope,f1.scope))]));
        table2 = squeeze(repmat(f2.table, [ones(1,length(f2.scope)) Nval(setdiff(f1.scope,f2.scope))]));

        T1 = [f1.scope setdiff(f2.scope,f1.scope)];
        T2 = [f2.scope setdiff(f1.scope,f2.scope)];
        [~,I1] = sort(T1);
        [~,I2] = sort(T2);

        % If there is only one element in the scope we don't need to
        % permute
        if length(I1) > 1
            table1 = permute(table1, I1);
            table2 = permute(table2, I2);
        end

        f.scope = union(f1.scope,f2.scope);

        if op == 0
            f.table = squeeze(table1 + table2);
        elseif op == 1
            f.table = squeeze(table1 .* table2);
        elseif op == 2
            f.table = squeeze(table1 ./ table2);
        else
            error('ERROR: this operation does not exist');
        end
    end


    %% We replace all NaN values in the table (NaN => 0 / 0) with 0
    f.table(isnan(f.table)) = 0;

    %% We normalize
    f = normalize(f);

end

%% marginalize - operation of marginalization : delete w from all the scopes
% op = 0 : sum-marginalization
% op = 1 : max-marginalization
% w : variable to be eliminated
% set_phi_w/set_mu_w : all the factors that depend on w
function [phi_agr, mu_agr, delta_w] = marginalize(set_phi_w, set_mu_w, w, Nval, op)

    delta_w = [];

    %% Compute big Pi
    pi.scope = [];
    pi.table = 1;
    for i=1:length(set_phi_w)
        pi = op_factors(pi, set_phi_w{i}, Nval, 1);
    end


    %% Compute big Sigma
    sigma.scope = [];
    sigma.table = 0;
    for i=1:length(set_mu_w)
        sigma = op_factors(sigma, set_mu_w{i}, Nval, 0);
    end

    %% Compute the marginalized tables
    dim_phi_w = find(pi.scope == w);
    if ~isempty(dim_phi_w)
        if op == 0
            phi_agr.table = squeeze(sum(pi.table, dim_phi_w));
        elseif op == 1
            phi_agr.table = squeeze(max(pi.table, [], dim_phi_w));
        else
            error('ERROR: this type of marginalization does not exist');
        end
    else
        phi_agr.table = 1;
    end

    pi_sigma = op_factors(pi, sigma, Nval, 1); % Pi * Sigma
    dim_mu_w = find(pi_sigma.scope == w);
    if ~isempty(dim_mu_w)
        if op == 0
            mu_agr.table = squeeze(sum(pi_sigma.table, dim_mu_w));
        elseif op == 1
            delta_w.scope = setdiff(pi_sigma.scope, w);
            [mu_agr.table,delta_w.table] = max(pi_sigma.table, [], dim_mu_w);
            mu_agr.table = squeeze(mu_agr.table);
        else
            error('ERROR: this type of marginalization does not exist');
        end
    else
        mu_agr.table = 0;
    end

    %% Compute the scopes
    phi_agr.scope = [];
    for i=1:length(set_phi_w)
        phi_agr.scope = union(phi_agr.scope, set_phi_w{i}.scope);
    end
    phi_agr.scope = setdiff(phi_agr.scope, w);

    mu_agr.scope = phi_agr.scope;
    for i=1:length(set_mu_w)
        mu_agr.scope = union(mu_agr.scope, set_mu_w{i}.scope);
    end
    mu_agr.scope = setdiff(mu_agr.scope, w);



    %% We normalize
    phi_agr = normalize(phi_agr);
    mu_agr = normalize(mu_agr);
    
end





