function [policy, meu, cpu_time] = id_backward_induction(I)


%% id_backward_induction  Resolution of influence diagram with  
%                        decision tree backward algorithm
% Arguments ---------------------------------------------------------------
% Let N be the number of variable
% I (cell array) : influence diagram 
%   I.G (NxN) : adjacence graph (variables relation)
%   I.Ntype (N) : variable type
%   I.Nlabel (N) : variable label
%   I.Nval (N) : variable domain size
%   I.CPD (cell(N)) : conditional probability distribution
%   I.V (cell(N)) : utility function
% Evaluation --------------------------------------------------------------
  % Let n be the number of decision variable
%   policy(cell(N)) : optimal policy
%   meu         : optimal expected utility
%   cpu_time    : used CPU time
%--------------------------------------------------------------------------


%% initialization
cpu_time = cputime;


% check of arguments
if ~id_checkI(I)
  display 'ERROR: the elements of the diagram influence I are not consistent.'
else
  %% Create decision tree
  set_tree_constants();
  T = make_tree([], I);
  
  %% compute conditional probabilities for nodes under a nature nodes
  last_nature_variable = find(I.Ntype==1, 1, 'last');
  for n=size(T,1):-1:1 % from leave nodes to root node
      if T(n,c_parent_var) == last_nature_variable
          val = get_parents_values(n,T,I); % get parents value in the tree
          p = 1;
          for v=1:sum(I.Ntype~=3)
              if I.Ntype(v)==1 % a nature variable
                  x = I.G(I.Ntype~=3,v)'; % non utility variable influencing v
                  val_par = val(find(x(1:length(val)))); % get parents value in influence diagram
                  p = p * get_value(I.CPD{v}, [val_par val(v)]);
              end
          end
          T( n, c_p ) = p; % join probability
      else
          if I.Ntype( T(n, c_var) ) == 1 %T(n,c_var) == last_nature_variable 
              % update probability of children
              n_children = T(n,c_first_child:c_first_child+I.Nval(T(n,c_var))-1); % set of children
              p = sum( T(n_children, c_p) );
              for i = 1:length(n_children)
                   if p ~= 0
                       T(n_children(i), c_p) = T(n_children(i), c_p) / p;
                   end
              T(n, c_p) = p;
              end
          else % a decision node
              t = T(n, c_first_child);
              if t ~= 0
                  T(n, c_p) = T(t, c_p);
              end
          end 
      end
  end    

  
  %% compute utility 
  %  Utility for leaves nodes and maximum expected utility for other nodes
  for n=size(T,1):-1:1 % from leave nodes to root node
      if T(n,c_first_child) == 0 % a leaf node : sum utilities
          val = get_parents_values(n,T,I);
          v_utilities = find(I.Ntype==3);
          for i=1:length(v_utilities) 
              % for each utility variable compute utility
              v = v_utilities(i);
              % find values of preceeding decision and nature variables
              valu = val(find(I.G(I.Ntype~=3,v))); 
              T(n,c_meu) = T(n,c_meu) + get_value(I.V{v}, valu);
          end
      elseif I.Ntype(T(n,c_var)) == 2 
          % decision variable: maximum expected utility of children
          n_children = T(n,c_first_child:c_first_child+I.Nval(T(n,c_var))-1); 
          T(n,c_meu) = max(T(n_children,c_meu));
      elseif  I.Ntype(T(n,c_var)) == 1 
          % nature variable: weighted sum of utility of children
          n_children = T(n,c_first_child:c_first_child+I.Nval(T(n,c_var))-1); 
          T(n,c_meu) = sum(T(n_children,c_p).*T(n_children,c_meu));
      end
  end    
  
  
  %% find optimum policy 
  %  (values for decision variable with maximum expected utility)  
  %  create the structure for policy
  N = length(I.Ntype); % number of variables
  policy = cell(1,N);
  for v=1:N
      if I.Ntype(v)==2  % decision node 
          % create an array with a dimension for each preceeding variable in the list
          if v == 1 %first variable in the ordered list
              policy{v} = 0;
          elseif v == 2
              policy{v} = zeros(I.Nval(1),1);
          else
              policy{v} = zeros(I.Nval(1:v-1)); 
          end
      end
  end
  %  fill for decision variables
  for n=1:size(T,1)
      v = T(n,c_var);
      if I.Ntype(v)==2 % decision node
          val = get_parents_values(n,T,I); % values of preceeding variables
          n_children = T(n,c_first_child:c_first_child+I.Nval(T(n,c_var))-1);
          [~, d] = max(T(n_children,c_meu));
          policy{v} = set_value(policy{v}, val, d);
      end
  end
  meu = T(1,c_meu); % read the maximum expected utility in the root node
end;


cpu_time = cputime - cpu_time;
  


%% find the value of preeceding variables in the tree
function val = get_parents_values(n,T,I)

set_tree_constants();

v = T(n,c_parent_var);
if v ~= 0  % not the root node or a utility variable
    val = [get_parents_values(T(n,c_parent_index),T,I) T(n,c_parent_value)];
else
    val =[];
end


%% Get the value of 'v(c)'
%  Get an element of an array with the vector of coordinates
function x = get_value(v, c)
L = length(c);
if L <=0
  x = [];
else
  s = size(v);
  ind = c(1);
  for i=2:L
     ind = ind + (c(i)-1)*prod(s(1:i-1));
  end
  v = reshape(v,prod(s),1);
  x = v(ind);
end


 
%% Set the value d at 'v(c)' 
function V = set_value(v, c, d)
%  Set an element of an array with the vector of coordinates
L = length(c);
if L == 0
  V = d;
else
  s = size(v);
  V = reshape(v,[prod(s) 1]);
  ind = c(1);
  for i=2:L
     ind = ind + (c(i)-1)*prod(s(1:i-1));
  end
  V(ind) = d;
  V = reshape(V,s);
end