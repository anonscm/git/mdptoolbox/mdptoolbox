function I = id_example_student()

% id_example_entrepreneur Generate an example influence diagram
% For more detail, see Probabilistic Graphical Models.
% D. Koller and N. Friedman. MIT Press 2009.

% Variables type, 1: nature, 2: decision, 3: utility
Ntype = [1 1 2 1 1 2 1 1 3];
N = size(Ntype,2); % number of variables

% Variables label
Nlabel = cell(1,N);
Nlabel{1} = 'Intelligence';
Nlabel{2} = 'Rumors';
Nlabel{3} = 'Take';
Nlabel{4} = 'Difficulty';
Nlabel{5} = 'Grade';
Nlabel{6} = 'Apply';
Nlabel{7} = 'Letter';
Nlabel{8} = 'Job';
Nlabel{9} = 'Utility';

% Domain size
Nval = [2 2 2 2 2 2 2 2 0];

% Variable relation: DAG
% G = zeros(N,N);
% G([1 2], 3) = 1;
% G(4, [2 5 6]) = 1;
% G(3, 6) = 1;
% G(5, [6 7]) = 1;
% G([6 7], 8) = 1;
% G(8, 9) = 1;

G = zeros(N,N);
G(1, [3 5]) = 1;
G(2, 3) = 1;
G(3, [5 6]) = 1;
G(4, [2 5 6]) = 1;
G(5, [6 7]) = 1;
G(6, 8) = 1;
G(7, 8) = 1;
G(8, 9) = 1;

% Conditional probabilities distributions
CPD = cell(1,N);
% the vector is ranged varying V1 and at last Vn
% For example for variables V1, V2 influencibg V3 with 2 values: 1,2
% V1 V2 V3
% 1 1 1
% 2 1 1
% 1 2 1
% 2 2 1
% 1 1 2
% 2 1 2
% 1 2 2
% 2 2 2
CPD{1} = reshape([0.5 0.5],[2 1]);
CPD{2} = reshape([0.5 0.5 0.5 0.5],[2 2]);
CPD{4} = reshape([0.5 0.5],[2 1]);
CPD{5} = reshape([0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5],[2 2 2 2]);
CPD{7} = reshape([0.5 0.5 0.5 0.5],[2 2]);
CPD{8} = reshape([0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5],[2 2 2]);

% Utilities
V = cell(1,N);
V{9} = reshape([10 100000],[2 1]);

% Influence diagram
I.G = G;
I.Ntype = Ntype;
I.Nlabel = Nlabel;
I.Nval = Nval;
I.CPD = CPD;
I.V= V;

%view(biograph(I.G))

end
