function TEST()

// Test of MDP toolbox functions
// In the MDPtoolbox directory, just type: TEST
// Then check that all tests are passed !

// MDPtoolbox: Markov Decision Processes Toolbox
// Copyright (C) 2009  INRA
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, 
//      this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright notice, 
//      this list of conditions and the following disclaimer in the documentation 
//      and/or other materials provided with the distribution.
//    * Neither the name of the <ORGANIZATION> nor the names of its contributors 
//      may be used to endorse or promote products derived from this software 
//      without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.


// some tests do not pass in non MATLAB environment (randi, linprog functions
// not defined, different random number generator)
ENVIRONMENT = 'MATLABk';
//ENVIRONMENT = 'other';


P(:,:,1) = [ 0.5448,    0.4552,    0; 
	     0.4344,    0.3033,    0.2623; 
	     0.1074,    0.0825,    0.8101];
P(:,:,2) = [ 0.4675,    0.4638,    0.0687; 
	     0.1480,    0.4559,    0.3961; 
	     0.3612,    0.2979,    0.3409];
R(:,:,1) = [ 0,   -0.9286,    0.3575; 
	     0,    0.6983,    0.5155; 
	     0,    0.8680,    0.4863];
R(:,:,2) = [ -0.2155,    0.4121,   -0.9077; 
	      0.3110,   -0.9363,   -0.8057; 
	     -0.6576,   -0.4462,    0.6469];
P_error=P; P_error(1,1,1)=0;
P_sparse=list();
P_sparse(1)=sparse(P(:,:,1));
P_sparse(2)=sparse(P(:,:,2));
R_sparse=list()
R_sparse(1)=sparse(R(:,:,1));
R_sparse(2)=sparse(R(:,:,2));
PR = [ -0.4227,    0.0280;
	0.3470,   -0.7000;
	0.4656,   -0.1499];
PR_sparse = sparse(PR);
discount = 0.8;

disp 'Test 1. on mdp_check_square_stochastic:' 
if ~isempty(mdp_check_square_stochastic(P(:,:,1)))
 disp '   * Test 1.1 FAILED'; else disp '    Test 1.1 passed'; end
if mdp_check_square_stochastic(P_error(:,:,1)) ~= 'MDP Toolbox ERROR: Row sums of the matrix must be 1'
 disp '   * Test 1.2 FAILED'; else disp '    Test 1.2 passed'; end



disp 'Test 2. on mdp_check:' 
if ~isempty(mdp_check(P,R))
 disp '   * Test 2.1 FAILED'; else disp '    Test 2.1 passed'; end
if mdp_check(P_error,R) ~= 'MDP Toolbox ERROR: Row sums of the matrix must be 1'
 disp '   * Test 2.2 FAILED'; else disp '    Test 2.2 passed'; end
if mdp_check(P,R(:,:,1)) ~= 'MDP Toolbox ERROR: Incompatibility between P and R dimensions'
 disp '   * Test 2.3 FAILED'; else disp '    Test 2.3 passed'; end
if ~isempty(mdp_check(P_sparse,R_sparse))
 disp '   * Test 2.4 FAILED'; else disp '    Test 2.4 passed'; end
if ~isempty(mdp_check(P,PR))
 disp '   * Test 2.5 FAILED'; else disp '    Test 2.5 passed'; end
if ~isempty(mdp_check(P,PR_sparse))
 disp '   * Test 2.6 FAILED'; else disp '    Test 2.6 passed'; end



disp 'Test 3. on mdp_value_iteration_bound_iter:'
 if mdp_value_iteration_bound_iter(P, R, discount, .01, [0;0;0]) ~= 12
 disp '   * Test 3.1 FAILED'; else disp '    Test 3.1 passed'; end
if mdp_value_iteration_bound_iter(P_sparse, R, discount, .01, [0;0;0]) ~= 12
 disp '   * Test 3.2 FAILED'; else disp '    Test 3.2 passed'; end
if mdp_value_iteration_bound_iter(P_sparse, R_sparse, discount, .01, [0;0;0]) ~= 12
 disp '   * Test 3.3 FAILED'; else disp '    Test 3.3 passed'; end
if mdp_value_iteration_bound_iter(P, PR, discount, .01, [0;0;0]) ~= 12
 disp '   * Test 3.4 FAILED'; else disp '    Test 3.4 passed'; end
if mdp_value_iteration_bound_iter(P_sparse, PR_sparse, discount, .01, [0;0;0]) ~= 12
 disp '   * Test 3.5 FAILED'; else disp '    Test 3.5 passed'; end
if mdp_value_iteration_bound_iter(P, R,discount,.0001, [0;0;0]) ~= 23
 disp '   * Test 3.6 FAILED'; else disp '    Test 3.6 passed'; end



disp 'Test 4. on mdp_computePpolicyPRpolicy:'
Ppolicy = [ 0.5448,    0.4552,         0;
	    0.1480,    0.4559,    0.3961;
	    0.3612,    0.2979,    0.3409];
PRpolicy = [ -0.4227; -0.7000; -0.1499];
[Pp, PRp] = mdp_computePpolicyPRpolicy(P, PR, [1;2;2]);
if max(max(abs( Pp - Ppolicy ))) > 0.001 | max(abs( PRp - PRpolicy )) > 0.001
 disp '   * Test 4.1 FAILED'; else disp '    Test 4.1 passed'; end
[Pp, PRp] = mdp_computePpolicyPRpolicy(P_sparse, R, [1;2;2]);
if max(max(abs( Pp - Ppolicy ))) > 0.001 | max(abs( PRp - PRpolicy )) > 0.001
 disp '   * Test 4.2 FAILED'; else disp '    Test 4.2 passed'; end
[Pp, PRp] = mdp_computePpolicyPRpolicy(P_sparse, R_sparse, [1;2;2]);
if max(max(abs( Pp - Ppolicy ))) > 0.001 | max(abs( PRp - PRpolicy )) > 0.001
 disp '   * Test 4.3 FAILED'; else disp '    Test 4.3 passed'; end
[Pp, PRp] = mdp_computePpolicyPRpolicy(P, R, [1;2;2]);
if max(max(abs( Pp - Ppolicy ))) > 0.001 | max(abs( PRp - PRpolicy )) > 0.001
 disp '   * Test 4.4 FAILED'; else disp '    Test 4.4 passed'; end
[Pp, PRp] = mdp_computePpolicyPRpolicy(P, R_sparse, [1;2;2]);
if max(max(abs( Pp - Ppolicy ))) > 0.001 | max(abs( PRp - PRpolicy )) > 0.001
 disp '   * Test 4.5 FAILED'; else disp '    Test 4.5 passed'; end

 

disp 'Test 5. on mdp_computePR:'
if max(max(abs(mdp_computePR(P, R) - PR ))) > 0.001
 disp '   * Test 5.1 FAILED'; else disp '    Test 5.1 passed'; end
if max(max(abs(mdp_computePR(P_sparse, R) - PR ))) > 0.001
 disp '   * Test 5.2 FAILED'; else disp '    Test 5.2 passed'; end
if max(max(abs(mdp_computePR(P_sparse, R_sparse) - PR ))) > 0.001
 disp '   * Test 5.3 FAILED'; else disp '    Test 5.3 passed'; end
if max(max(abs(mdp_computePR(P, PR) - PR ))) > 0.001
 disp '   * Test 5.4 FAILED'; else disp '    Test 5.4 passed'; end
if max(max(abs(mdp_computePR(P_sparse, PR_sparse) - PR ))) > 0.001
 disp '   * Test 5.5 FAILED'; else disp '    Test 5.5 passed'; end



disp 'Test 6. on mdp_bellman_operator:'
V = [0.8830; 1.3568; 1.9137];
policy = [2;1;1];
[v, p] = mdp_bellman_operator(P, PR, discount,[1;1;2]);
if max(abs( v - V )) > 0.001 | or(or(or(p ~= policy))) 
 disp '   * Test 6.1 FAILED'; else disp '    Test 6.1 passed'; end
[v, p] = mdp_bellman_operator(P_sparse, PR_sparse, discount,[1;1;2]);
if max(abs( v - V )) > 0.001 | or(or(p ~= policy)) 
 disp '   * Test 6.2 FAILED'; else disp '    Test 6.2 passed'; end



disp 'Test 7. on mdp_eval_policy_iterative:'
V = [0.5217; 0.7674; 0.2525];
if max(abs(mdp_eval_policy_iterative(P, PR, discount, [2;1;2], [0;0;0], .0001, 10000) - V )) > 0.001
 disp '   * Test 7.1 FAILED'; else disp '    Test 7.1 passed'; end
if max(abs(mdp_eval_policy_iterative(P_sparse, R, discount, [2;1;2], [0;0;0], .0001, 10000) - V )) > 0.001
 disp '   * Test 7.2 FAILED'; else disp '    Test 7.2 passed'; end
if max(abs(mdp_eval_policy_iterative(P_sparse, R_sparse, discount, [2;1;2], [0;0;0], .0001, 10000) - V )) > 0.001
 disp '   * Test 7.3 FAILED'; else disp '    Test 7.3 passed'; end
if max(abs(mdp_eval_policy_iterative(P, R, discount, [2;1;2], [0;0;0], .0001, 10000) - V )) > 0.001
 disp '   * Test 7.4 FAILED'; else disp '    Test 7.4 passed'; end
if max(abs(mdp_eval_policy_iterative(P_sparse, PR_sparse, discount, [2;1;2], [0;0;0], .0001, 10000) - V )) > 0.001
 disp '   * Test 7.5 FAILED'; else disp '    Test 7.5 passed'; end
V = [0.6140; 0.8598; 0.3449];
if max(abs(mdp_eval_policy_iterative(P, R_sparse, discount, [2;1;2], [1;1;1], 0.1, 10) - V )) > 0.001
 disp '   * Test 7.6 FAILED'; else disp '    Test 7.6 passed'; end


disp 'Test 8. on mdp_eval_policy_matrix:'
V = [0.5220; 0.7677; 0.2529];
if max(abs(mdp_eval_policy_matrix(P, R, discount, [2;1;2]) - V )) > 0.001
 disp '   * Test 8.1 FAILED'; else disp '    Test 8.1 passed'; end
if max(abs(mdp_eval_policy_matrix(P_sparse, R, discount, [2;1;2]) - V )) > 0.001
 disp '   * Test 8.2 FAILED'; else disp '    Test 8.2 passed'; end
if max(abs(mdp_eval_policy_matrix(P_sparse, R_sparse, discount, [2;1;2]) - V )) > 0.001
 disp '   * Test 8.3 FAILED'; else disp '    Test 8.3 passed'; end


if ENVIRONMENT == 'MATLAB'
  disp 'Test 9. on mdp_eval_policy_TD_0:'
  V = [- 0.7148483; 2.5928424; 2.6447583];
  grand('setsd',ones(625,1))
  if max(abs(V - mdp_eval_policy_TD_0(P,R,discount, [2;1;2], 10000))) > 0.001
   disp '   * Test 9.1 FAILED'; else disp '    Test 9.1 passed'; end
  grand('setsd',ones(625,1))
  if max(abs(V - mdp_eval_policy_TD_0(P_sparse,R,discount, [2;1;2], 10000))) > 0.001
   disp '   * Test 9.2 FAILED'; else disp '    Test 9.2 passed'; end
  grand('setsd',ones(625,1))
  if max(abs(V - mdp_eval_policy_TD_0(P_sparse,R_sparse,discount, [2;1;2], 10000))) > 0.001
   disp '   * Test 9.3 FAILED'; else disp '    Test 9.3 passed'; end
  V = [0.2239419; 1.2696016; - 0.5422791];
  grand('setsd',ones(625,1))
  if max(abs(V - mdp_eval_policy_TD_0(P,PR,discount, [2;1;2], 10000))) > 0.001
   disp '   * Test 9.4 FAILED'; else disp '    Test 9.4 passed'; end
  grand('setsd',ones(625,1))
  if max(abs(V - mdp_eval_policy_TD_0(P_sparse,PR_sparse,discount, [2;1;2], 10000))) > 0.001
   disp '   * Test 9.5 FAILED'; else disp '    Test 9.5 passed'; end
  V = [- 0.4588619; 2.8613588; 2.5149332]; 
  grand('setsd',ones(625,1))
  if max(abs(V - mdp_eval_policy_TD_0(P,R,discount, [2;1;2], 15000))) > 0.001
   disp '   * Test 9.6 FAILED'; else disp '    Test 9.6 passed'; end
end

disp 'Test 10. on mdp_eval_policy_optimality:'
V = [1.0790; 1.4692; 1.8619];
optimal_actions = [%F, %T; %T, %F; %T, %F];
[nil, o] = mdp_eval_policy_optimality(P, R, discount, V);
if or(o ~= optimal_actions)
 disp '   * Test 10.1 FAILED'; else disp '    Test 10.1 passed'; end
[nil, o] = mdp_eval_policy_optimality(P_sparse, R, discount, V);
if or(o ~= optimal_actions)
 disp '   * Test 10.2 FAILED'; else disp '    Test 10.2 passed'; end
[nil, o] = mdp_eval_policy_optimality(P_sparse, R_sparse, discount, V);
if or(o ~= optimal_actions)
 disp '   * Test 10.3 FAILED'; else disp '    Test 10.3 passed'; end
[nil, o] = mdp_eval_policy_optimality(P, PR, discount, V);
if or(o ~= optimal_actions)
 disp '   * Test 10.4 FAILED'; else disp '    Test 10.4 passed'; end
[nil, o] = mdp_eval_policy_optimality(P_sparse, PR_sparse, discount, V);
if or(o ~= optimal_actions)
 disp '   * Test 10.5 FAILED'; else disp '    Test 10.5 passed'; end



disp 'Test 11. on mdp_finite_horizon:'
V=[0.4770,    0.3436,    0.1929,    0.0280,         0;
   0.8554,    0.7110,    0.5386,    0.3470,         0;
   1.2104,    1.0313,    0.7926,    0.4656,         0];
policy = [ 2,     2,     2,     2;
	   1,     1,     1,     1;
	   1,     1,     1,     1];
[v, p] = mdp_finite_horizon(P, R, discount, 4, [0;0;0]);
if max(max(abs(V - v))) > 0.001 | or(or(or(p ~= policy)))
 disp '   * Test 11.1 FAILED'; else disp '    Test 11.1 passed'; end
[v, p] = mdp_finite_horizon(P_sparse, R, discount, 4, [0;0;0]);
if max(max(abs(V - v))) > 0.001 | or(or(p ~= policy))
 disp '   * Test 11.2 FAILED'; else disp '    Test 11.2 passed'; end
[v, p] = mdp_finite_horizon(P_sparse, R_sparse, discount, 4, [0;0;0]);
if max(max(abs(V - v))) > 0.001 | or(or(p ~= policy))
 disp '   * Test 11.3 FAILED'; else disp '    Test 11.3 passed'; end
[v, p] = mdp_finite_horizon(P, PR, discount, 4, [0;0;0]);
if max(max(abs(V - v))) > 0.001 | or(or(p ~= policy))
 disp '   * Test 11.4 FAILED'; else disp '    Test 11.4 passed'; end
[v, p] = mdp_finite_horizon(P_sparse, PR_sparse, discount, 4, [0;0;0]);
if max(max(abs(V - v))) > 0.001 | or(or(p ~= policy))
 disp '   * Test 11.5 FAILED'; else disp '    Test 11.5 passed'; end
V = [0.8866,    0.8556,    0.8329,    0.8280,    1.0000;
     1.2650,    1.2230,    1.1786,    1.1470,    1.0000;
     1.6200,    1.5433,    1.4326,    1.2656,    1.0000];
policy = [2,     2,     2,     2;
          1,     1,     1,     1;
          1,     1,     1,     1];
[v, p] = mdp_finite_horizon(P, R, discount, 4, [1;1;1]);
if max(max(abs(V - v))) > 0.001 | or(or(p ~= policy))
 disp '   * Test 11.6 FAILED'; else disp '    Test 11.6 passed'; end



disp 'Test 12. on mdp_value_iteration:'
//V = [0.8744; 1.2641; 1.6554]; // last V was previouly returned
policy = [2;1;1];
[p] = mdp_value_iteration(P, R, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 12.1 FAILED'; else disp '    Test 12.1 passed'; end
[p] = mdp_value_iteration(P_sparse, R, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 12.2 FAILED'; else disp '    Test 12.2 passed'; end
[p] = mdp_value_iteration(P_sparse, R_sparse, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 12.3 FAILED'; else disp '    Test 12.3 passed'; end
[p] = mdp_value_iteration(P, PR, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 12.4 FAILED'; else disp '    Test 12.4 passed'; end
[p] = mdp_value_iteration(P_sparse, PR_sparse, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 12.5 FAILED'; else disp '    Test 12.5 passed'; end
//V = [1.0426; 1.4327; 1.8253]; // last V was previouly returned
policy = [2;1;1];
[p] = mdp_value_iteration(P, PR, discount, 0.001, 17, [1;1;1]);
if or(or(p ~= policy))
 disp '   * Test 12.6 FAILED'; else disp '    Test 12.6 passed'; end



disp 'Test 13. on mdp_value_iterationGS:'
//V = [1.0284; 1.4234; 1.8126];  // last V was previouly returned
policy = [2;1;1];
[p] = mdp_value_iterationGS(P, R, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 13.1 FAILED'; else disp '    Test 13.1 passed'; end
[p] = mdp_value_iterationGS(P_sparse, R, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 13.2 FAILED'; else disp '    Test 13.2 passed'; end
[p] = mdp_value_iterationGS(P_sparse, R_sparse, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 13.3 FAILED'; else disp '    Test 13.3 passed'; end
[p] = mdp_value_iterationGS(P, PR, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 13.4 FAILED'; else disp '    Test 13.4 passed'; end
[p] = mdp_value_iterationGS(P_sparse, PR_sparse, discount, .01, 12, [0;0;0]);
if or(or(p ~= policy))
 disp '   * Test 13.5 FAILED'; else disp '    Test 13.5 passed'; end
//V = [1.0738; 1.4645; 1.8569];  // last V was previouly returned
policy = [2;1;1];
[p] = mdp_value_iterationGS(P, PR, discount, 0.001, 17, [1;1;1]);
if or(or(p ~= policy))
 disp '   * Test 13.6 FAILED'; else disp '    Test 13.6 passed'; end



disp 'Test 14. on mdp_policy_iteration:'
V = [1.0790; 1.4692; 1.8619];
policy = [2;1;1];
[v, p] = mdp_policy_iteration(P, R, discount, [2;1;1], 1000, 0);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 14.1 FAILED'; else disp '    Test 14.1 passed'; end
[v, p] = mdp_policy_iteration(P_sparse, R, discount, [2;1;1], 1000, 0);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 14.2 FAILED'; else disp '    Test 14.2 passed'; end
[v, p] = mdp_policy_iteration(P_sparse, R_sparse, discount, [2;1;1], 1000, 0);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 14.3 FAILED'; else disp '    Test 14.3 passed'; end
[v, p] = mdp_policy_iteration(P, PR, discount, [2;1;1], 1000, 0);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 14.4 FAILED'; else disp '    Test 14.4 passed'; end
[v, p] = mdp_policy_iteration(P_sparse, PR_sparse, discount, [2;1;1], 1000, 0);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 14.5 FAILED'; else disp '    Test 14.5 passed'; end
V = [-2.3867; -2.5370; -1.9856];
policy = [1;2;2];
[v, p] = mdp_policy_iteration(P, R, discount, [1;2;2], 1, 1);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 14.6 FAILED'; else disp '    Test 14.6 passed'; end



disp 'Test 15. on mdp_policy_iteration_modified:'
 V = [0.7334; 1.1235; 1.5160];
 policy = [2;1;1];
[v, p] = mdp_policy_iteration_modified(P, R, discount, .01, 10);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 15.1 FAILED'; else disp '    Test 15.1 passed'; end
[v, p] = mdp_policy_iteration_modified(P_sparse, R, discount, .01, 10);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 15.2 FAILED'; else disp '    Test 15.2 passed'; end
[v, p] = mdp_policy_iteration_modified(P_sparse, R_sparse, discount, .01, 10);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 15.3 FAILED'; else disp '    Test 15.3 passed'; end
[v, p] = mdp_policy_iteration_modified(P, PR, discount, .01, 10);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 15.4 FAILED'; else disp '    Test 15.4 passed'; end
[v, p] = mdp_policy_iteration_modified(P_sparse, PR_sparse, discount, .01, 10);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 15.5 FAILED'; else disp '    Test 15.5 passed'; end
V = [1.0065; 1.3967; 1.7894];
policy = [2;1;1];
[v, p] = mdp_policy_iteration_modified(P, R, discount, .0001, 5);
if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
 disp '   * Test 15.6 FAILED'; else disp '    Test 15.6 passed'; end


if ENVIRONMENT == 'MATLAB'
  disp 'Test 16. on mdp_LP:'
  V = [1.0790; 1.4692; 1.8619];
  policy = [2;1;1];
  [v, p] = mdp_LP(P, R, discount);
  if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
   disp '   * Test 16.1 FAILED'; else disp '    Test 16.1 passed'; end
  [v, p] = mdp_LP(P_sparse, R, discount);
  if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
   disp '   * Test 16.2 FAILED'; else disp '    Test 16.2 passed'; end
  [v, p] = mdp_LP(P_sparse, R_sparse, discount);
  if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
   disp '   * Test 16.3 FAILED'; else disp '    Test 16.3 passed'; end
  [v, p] = mdp_LP(P, PR, discount);
  if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
   disp '   * Test 16.4 FAILED'; else disp '    Test 16.4 passed'; end
  [v, p] = mdp_LP(P_sparse, PR_sparse, discount);
  if or(max(abs(V - v )) > 0.001) | or(or(p ~= policy))
   disp '   * Test 16.5 FAILED'; else disp '    Test 16.5 passed'; end
end


disp 'Test 17. on mdp_relative_value_iteration:'
policy = [2;1;1];
average_reward = 0.3029;
[p, ar] = mdp_relative_value_iteration(P, R, .01, 1000);
if or(or(p ~= policy)) | abs(average_reward - ar) > 0.001 
 disp '   * Test 17.1 FAILED'; else disp '    Test 17.1 passed'; end
[p, ar] = mdp_relative_value_iteration(P, R, .01, 1000);
if or(or(p ~= policy)) | abs(average_reward - ar) > 0.001 
 disp '   * Test 17.2 FAILED'; else disp '    Test 17.2 passed'; end
[p, ar] = mdp_relative_value_iteration(P, R, .01, 1000);
if or(or(p ~= policy)) | abs(average_reward - ar) > 0.001 
 disp '   * Test 17.3 FAILED'; else disp '    Test 17.3 passed'; end
[p, ar] = mdp_relative_value_iteration(P, R, .01, 1000);
if or(or(p ~= policy)) | abs(average_reward - ar) > 0.001 
 disp '   * Test 17.4 FAILED'; else disp '    Test 17.4 passed'; end
[p, ar] = mdp_relative_value_iteration(P, R, .01, 1000);
if or(or(p ~= policy)) | abs(average_reward - ar) > 0.001 
 disp '   * Test 17.5 FAILED'; else disp '    Test 17.5 passed'; end
policy = [2;1;1];
average_reward = 0.2766;
[p, ar] = mdp_relative_value_iteration(P, R, 0.001, 5);
if or(or(p ~= policy)) | abs(average_reward - ar) > 0.001 
 disp '   * Test 17.6 FAILED'; else disp '    Test 17.6 passed'; end


if ENVIRONMENT == 'MATLAB'
  disp 'Test 18. on mdp_Q_learning:'
  Q = [0.2102090,    0.1986696;
       2.4563298,    0.2782959;
       2.3231481,    2.350429];
  V = [0.2102090; 2.4563298; 2.3504294]; 
  policy_q1 = [2;1;1];
  means = [0.,0.,0.,0.0205427,0.0016312,0.0183897,0.0137610,0.0069412,0.0036113,0.0041935];
  grand('setsd',ones(625,1))
  [q, v, p, m] = mdp_Q_learning(P, R, discount, 10000);
  if max(max(abs(Q - q ))) > 0.001 | max(abs(V - v )) > 0.001 | or(or(p ~= policy_q1)) | max(abs(means - m(1:10))) > 0.001
   disp '   * Test 18.1 FAILED'; else disp '    Test 18.1 passed'; end
  grand('setsd',ones(625,1))
  [q, v, p, m] = mdp_Q_learning(P_sparse, R, discount, 10000);
  if max(max(abs(Q - q ))) > 0.001 | max(abs(V - v )) > 0.001 | or(or(p ~= policy_q1)) | max(abs(means - m(1:10))) > 0.001
   disp '   * Test 18.2 FAILED'; else disp '    Test 18.2 passed'; end
  grand('setsd',ones(625,1))
  [q, v, p, m] = mdp_Q_learning(P_sparse, R_sparse, discount, 10000);
  if max(max(abs(Q - q ))) > 0.001 | max(abs(V - v )) > 0.001 | or(or(p ~= policy_q1)) | max(abs(means - m(1:10))) > 0.001
   disp '   * Test 18.3 FAILED'; else disp '    Test 18.3 passed'; end
  Q = [- 0.0405634,    0.3873731;
       1.5497109,    0.1774197;
       2.1573326,    1.55504;];
  V = [0.3873731;1.5497109;2.1573326]; 
  policy_q2 = [2;1;1];
  means = [0.0061482,0.0003455,0.0002031,0.0014706,0.0002421,0.0064509,0.0074175,0.0091091,0.0016129,0.0048541];
  grand('setsd',ones(625,1))
  [q, v, p, m] = mdp_Q_learning(P, PR, discount, 10000);
  if max(max(abs(Q - q ))) > 0.001 | max(abs(V - v )) > 0.001 | or(or(p ~= policy_q2)) | max(abs(means - m(1:10))) > 0.001
   disp '   * Test 18.4 FAILED'; else disp '    Test 18.4 passed'; end
  grand('setsd',ones(625,1))
  [q, v, p, m] = mdp_Q_learning(P_sparse, PR_sparse, discount, 10000);
  if max(max(abs(Q - q ))) > 0.001 | max(abs(V - v )) > 0.001 | or(or(p ~= policy_q2)) | max(abs(means - m(1:10))) > 0.001
   disp '   * Test 18.5 FAILED'; else disp '    Test 18.5 passed'; end
  Q = [0.3706569,    0.8273256;
       1.5040853,    0.6029026;
       1.8568845,    1.4672491];
  V = [0.8273256; 1.5040853; 1.8568845]; 
  policy_q3 = [2;1;1];
  means = [0.0012758,0.0015749,0.0014146,0.0015998,0.0013375,0.0014329,0.0012685,0.0011292,0.0013682,0.0012027];
  grand('setsd',ones(625,1))
  [q, v, p, m] = mdp_Q_learning(P, R, discount, 100000);
  if max(max(abs(Q - q ))) > 0.001 | max(abs(V - v )) > 0.001 | or(or(p ~= policy_q3)) | max(abs(means - m(991:1000))) > 0.001
   disp '   * Test 18.6 FAILED'; else disp '    Test 18.6 passed'; end
end


if ENVIRONMENT == 'MATLAB'
  disp 'Test 19. on mdp_example_rand:'
  P(:,:,1) = [0.3333333,0.3333333,0.3333333;
              0.3333333,0.3333333,0.3333333;
              0.3333333,0.3333333,0.3333333];
  P(:,:,2) = [0.3333333,0.3333333,0.3333333;
              0.3333333,0.3333333,0.3333333;
              0.3333333,0.3333333,0.3333333];
  R(:,:,1) = [-0.9980468,-0.9980468,-0.9980468;
              -0.9980468,-0.9980468,-0.9980468;
              -0.9980468,-0.9980468,-0.9980468];
  R(:,:,2) = [-0.9980468,-0.9980468,-0.9980468;
              -0.9980468,-0.9980468,-0.9980468;
              -0.9980468,-0.9980468,-0.9980468];
  grand('setsd',ones(625,1))
  [p, r] = mdp_example_rand (3, 2, %F, ones(3,3));
  if max(max(abs(P(:,:,1) - p(:,:,1)))) > 0.001 | max(max(abs(P(:,:,2) - p(:,:,2)))) > 0.001 | ...
     max(max(abs(R(:,:,1) - r(:,:,1)))) > 0.001 | max(max(abs(R(:,:,2) - r(:,:,2)))) > 0.001
   disp '   * Test 19.1 FAILED'; else disp '    Test 19.1 passed'; end
  P_sparse(1) = sparse([0.5,0.5,0;
                        0.5,0.5,0;
                        0.5,0.5,0]);
  P_sparse(2) = sparse([0.5,0.5,0;
                        0.5,0.5,0;
                        0.5,0.5,0]);
  R_sparse(1) = sparse([-0.9980468,-0.9980468,0;
                        -0.9980468,-0.9980468,0;
                        -0.9980468,-0.9980468,0]);
  R_sparse(2) = sparse([-0.9980468,-0.9980468,0;
	                -0.9980468,-0.9980468,0;
		         -0.9980468,-0.9980468,0]);
  grand('setsd',ones(625,1))
  [p, r] = mdp_example_rand (3, 2, %T, [1,1,0;1,1,0;1,1,0]);
  if max(max(abs(P_sparse(1) - p(1)))) > 0.001 | max(max(abs(P_sparse(2) - p(2)))) > 0.001 | ...
     max(max(abs(R_sparse(1) - r(1)))) > 0.001 | max(max(abs(R_sparse(2) - r(2)))) > 0.001
   disp '   * Test 19.2 FAILED'; else disp '    Test 19.2 passed'; end
end


disp 'Test 20. on mdp_example_forest:'
P(:,:,1) = [0.1, 0.9, 0; 0.1, 0, 0.9; 0.1, 0, 0.9]; 
P(:,:,2) = [1, 0, 0; 1, 0, 0; 1, 0, 0];
R = [0, 0; 0, 1; 4, 2];
[p, r] = mdp_example_forest (3, 4, 2, .1);
if max(max(abs(P(:,:,1) - p(:,:,1)))) > 0.001 | max(max(abs(P(:,:,2) - p(:,:,2)))) > 0.001 | ...
   max(max(abs(R - r ))) > 0.001
  disp '   * Test 20.1 FAILED'; else disp '    Test 20.1 passed'; end
clear P R
P(:,:,1) = [0.4, 0.6, 0, 0; 0.4, 0, 0.6, 0; 0.4, 0, 0, 0.6; 0.4, 0, 0, 0.6]; 
P(:,:,2) = [1, 0, 0, 0; 1, 0, 0, 0; 1, 0, 0, 0; 1, 0, 0, 0];
R = [0, 0; 0, 1;  0, 1; 10, 20];
[p, r] = mdp_example_forest (4, 10, 20, 0.4);
if max(max(abs(P(:,:,1) - p(:,:,1)))) > 0.001 | max(max(abs(P(:,:,2) - p(:,:,2)))) > 0.001 | ...
   max(max(abs(R - r))) > 0.001
  disp '   * Test 20.2 FAILED'; else disp '    Test 20.2 passed'; end


endfunction
