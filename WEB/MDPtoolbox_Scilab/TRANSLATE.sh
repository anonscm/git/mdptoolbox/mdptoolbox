#!/usr/bin/env tcsh
# Launch the script with the following command: tcsh TRANSLATE.sh

# DIFFERENCES BETWEEN MATLAB AND SCILAB
# - change the files extension
# - change the comment flag from '%' to '//'
# - add 'endfunction at the end of each function
# - way of catching CPU time
# - cell structure vs list structure
# - nargin vs argn(2)
# - max arguments
# - random number generator
# - mod vs modulo
# - repmat does not exist in Scilab
# - linprog vs ?
# - num2str vs string
# - interpretation of zeros, ones, eye (S)
# - boolean values
# - cat does not have the same arguments
# - change any in or, and all in and
# - change || in |, and && in &
# - change strcmp in ==

# To avoid problem, some differences are taken into account in MATLAB code:
# - 2 dimensions are explicit for zeros, ones, eye call (for exemple eye(S,S) and not eye(S) )
# - boolean values used are only false and true (0 for false and true for other is not used) 
# - use isempty to test if empty and not if length is 0
# - avoid concatenation of matrices (old code of mdp_example_rand


# AUTOMATIC TRANSLATION
foreach f (`ls *.m`)
  echo $f
  # change the comment flag
  sed 's/%/\/\//g' $f > tmpfile; mv -f tmpfile $f
  # add 'endfunction at the end of each function
  sed '$a\endfunction' $f > tmpfile; mv -f tmpfile $f
  # change way of catching CPU time
  sed 's/cpu_time = cputime;/timer();/' $f > tmpfile; mv -f tmpfile $f
  sed 's/cpu_time = cputime - cpu_time;/cpu_time = timer();/' $f > tmpfile; mv -f tmpfile $f
  # change cell in list structure
  sed "s/iscell(P)/(typeof(P)=='list')/" $f > tmpfile; mv -f tmpfile $f
  sed "s/iscell(R)/(typeof(R)=='list')/" $f > tmpfile; mv -f tmpfile $f
  sed 's/{/(/g' $f > tmpfile; mv -f tmpfile $f
  sed 's/}/)/g' $f > tmpfile; mv -f tmpfile $f
  # change nargin in argn(2)
  sed 's/nargin/argn(2)/g' $f > tmpfile; mv -f tmpfile $f
  # change max arguments
  sed "s/max(Q,\[],2)/max(Q,'c')/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/max(Q2,\[],2)/max(Q2,'c')/g" $f > tmpfile; mv -f tmpfile $f
  # change call of random number generator
  sed "s/randi(\[1,S\])/grand(1,1,'uin',1,S)/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/randi(\[1,A\])/grand(1,1,'uin',1,A)/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/rand(1)/grand(1,1,'unf',0,1)/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/rand(S)/grand(S,S,'unf',0,1)/g" $f > tmpfile; mv -f tmpfile $f
  # change mod in pmodulo
  sed "s/mod(n/pmodulo(n/g" $f > tmpfile; mv -f tmpfile $f
  # replace repmat
  sed "s/repmat(Vpolicy,1,A)/ones(1,A).*.Vpolicy/g" $f > tmpfile; mv -f tmpfile $f
  # replace exist by exists
  sed "s/exist(/exists(/" $f > tmpfile; mv -f tmpfile $f
  # replace num2str by string
  sed "s/num2str/string/g" $f > tmpfile; mv -f tmpfile $f
  # avoid bad argument for string
  sed "s/string(iter,'\/\/5i')/string(iter)/g" $f > tmpfile; mv -f tmpfile $f
  # replace true by %T and false by %F
  sed "s/true/%T/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/false/%F/g" $f > tmpfile; mv -f tmpfile $f
  # change && and || (short circuit evaluation, compatible with  GNU Octave) in & and |
  sed "s/\&\&/\&/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/||/|/g" $f > tmpfile; mv -f tmpfile $f
  # change any in or
  sed "s/any/or/g" $f > tmpfile; mv -f tmpfile $f
  # change file extension
  mv $f $f:r.sci 
end

# Changes in TEST


# change reinitilization of random number generator and expected results
sed "s/rand('seed',0)/grand('setsd',ones(625,1))/g" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/V = \[0.5235; 0.6892; 0.1924\];/V = \[- 0.7148483; 2.5928424; 2.6447583\];/  " TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/V = \[0.5138; 0.7422; 0.2490];/V = \[0.2239419; 1.2696016; - 0.5422791];/  " TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/V = \[0.4779; 0.7876; 0.2501];/V = \[- 0.4588619; 2.8613588; 2.5149332]; /  " TEST.sci > tmpfile; mv -f tmpfile TEST.sci

sed "s/Q = \[0.5713,    1.1641;/Q = \[0.2102090,    0.1986696;/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/1.5426,    0.5283;/2.4563298,    0.2782959;/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/1.8846,    1.0597];/2.3231481,    2.350429];/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/V = \[1.1641; 1.5426; 1.8846];/V = \[0.2102090; 2.4563298; 2.3504294]; /  " TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/policy_q1 = [2;1;1];/policy_q1 = [1;1;2];/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/means = \[0.0824, 0.0303, 0.0252, 0.0191, 0.0195, 0.0175, 0.0160, 0.0117, 0.0132, 0.0135];/means = \[0.,0.,0.,0.0205427,0.0016312,0.0183897,0.0137610,0.0069412,0.0036113,0.0041935];/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci

sed "s/Q = \[0.5520, 1.0947;/Q = \[- 0.0405634,    0.3873731;/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/1.4818, 0.5247;/1.5497109,    0.1774197;/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/1.8573, 1.0216];/2.1573326,    1.55504;];/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/V = \[1.0947; 1.4818; 1.8573];/V = \[0.3873731;1.5497109;2.1573326]; /  " TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/means = \[0.0365, 0.0169, 0.0150, 0.0108, 0.0095, 0.0085, 0.0077, 0.0075, 0.0070, 0.0066];/means = \[0.0061482,0.0003455,0.0002031,0.0014706,0.0002421,0.0064509,0.0074175,0.0091091,0.0016129,0.0048541];/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci

sed "s/Q = \[0.5508, 1.0623;/Q = \[0.3706569,    0.8273256;/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/1.4764, 0.5362;/1.5040853,    0.6029026;/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/1.9084, 0.9667];/1.8568845,    1.4672491];/"  TEST.sci > tmpfile; mv -f tmpfile TEST.sci  
sed "s/V = \[1.0623; 1.4764; 1.9084];/V = \[0.8273256; 1.5040853; 1.8568845]; /  " TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/means = \[0.0010, 0.0011, 0.0012, 0.0009, 0.0012, 0.0013, 0.0012, 0.0013, 0.0013, 0.0013];/means = \[0.0012758,0.0015749,0.0014146,0.0015998,0.0013375,0.0014329,0.0012685,0.0011292,0.0013682,0.0012027];/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci

sed "s/0.1544, 0.4792, 0.3664/0.3333333,0.3333333,0.3333333/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.0260, 0.5156, 0.4584/0.3333333,0.3333333,0.3333333/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.6189, 0.3496, 0.0315/0.3333333,0.3333333,0.3333333/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.1117, 0.0161, 0.8722/0.3333333,0.3333333,0.3333333/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.3311, 0.2397, 0.4293/0.3333333,0.3333333,0.3333333/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.5058, 0.0504, 0.4439/0.3333333,0.3333333,0.3333333/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.8609,-0.8161, 0.4024/-0.9980468,-0.9980468,-0.9980468/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.6923, 0.3078, 0.8206/-0.9980468,-0.9980468,-0.9980468/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.0539,-0.1680, 0.5244/-0.9980468,-0.9980468,-0.9980468/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/-0.4751,-0.3435, 0.9821/-0.9980468,-0.9980468,-0.9980468/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/-0.9051, 0.2653,-0.2693/-0.9980468,-0.9980468,-0.9980468/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.4722, 0.5128,-0.5059/-0.9980468,-0.9980468,-0.9980468/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.2438, 0.7562, 0/0.5,0.5,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.0479, 0.9521, 0/0.5,0.5,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.6390, 0.3610, 0/0.5,0.5,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.8741, 0.1259, 0/0.5,0.5,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.5801, 0.4199, 0/0.5,0.5,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.9094, 0.0906, 0/0.5,0.5,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.8609,-0.8161, 0/-0.9980468,-0.9980468,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.6923, 0.3078, 0/-0.9980468,-0.9980468,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.0539,-0.1680, 0/-0.9980468,-0.9980468,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/-0.4751,-0.3435, 0/-0.9980468,-0.9980468,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/-0.9051, 0.2653, 0/-0.9980468,-0.9980468,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci
sed "s/0.4722, 0.5128, 0/-0.9980468,-0.9980468,0/" TEST.sci > tmpfile; mv -f tmpfile TEST.sci



# WHAT REMAINS TO TRANSLATE
# 1. Change file extension in MDP_PRINT
#
# 2. Variable initialization
#    * TEST : at the beginning add 
#             P_sparse=list();R_sparse=list();  (add)
#
#    * mdp_computePpolicyPRpolicy : add initialization of Ppolicy
#    if (typeof(P)=='list')
#        A = length(P);
#        S = size(P(1),1);  (add)
#    else
#        A = size(P,3);
#        S = size(P,1);  (add)
#    end
#    Ppolicy = zeros(S,S);  (add)
#
#    * mdp_example_rand
#       P=list();           (modify P=();)
#       R=list();           (modify R=();)
#
# 3. strcmp problem
#    * TEST : change function call with operator == or ~=
#
# 4. Change mod in modulo 
#    in mdp_policy_iteration.sci, mdp_eval_policy_TD_0.sci        
#       mdp_eval_policy_matrix.sci mdp_eval_policy_iterative.sci
#
# 5. Change all in and
#    in mdp_policy_iteration.sci
#




foreach f (`ls documentation/*.html`)
  echo $f
  # replace MATLAB by Scilab
  sed 's/MATLAB/Scilab/g' $f > tmpfile; mv -f tmpfile $f
  # replace true by %T and false by %F
  sed "s/true/%T/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/false/%F/g" $f > tmpfile; mv -f tmpfile $f
  # replace call to pseudorandom number generator
  sed "s/rand('seed',0)/grand('setsd',0)/g" $f > tmpfile; mv -f tmpfile $f
  # replace prompt of the environment
  sed "s/>>/->/g" $f > tmpfile; mv -f tmpfile $f
  # replace comment flag
  sed "s/% To be able/\/\//g" $f > tmpfile; mv -f tmpfile $f
  # replace name of list element
  sed "s/P{1}/P(1)/g" $f > tmpfile; mv -f tmpfile $f
  sed "s/P{2}/P(2)/g" $f > tmpfile; mv -f tmpfile $f
end

# WHAT REMAINS TO TRANSLATE
# 1. DOCUMENTATION.html 
#    developped with MATLAB by ... (remove the parenthesis)
#    update text
# 2. Update all examples
#    in mdp_example_rand.htlm, mdp_learning.htlm, mdp_eval_policy_TD_0.html
# 3. mdp_LP is not available
#    * remove file mdp_LP.html
#    * remove link to mdp_LP in files index_category.html and index_alphabetic.html
# 
