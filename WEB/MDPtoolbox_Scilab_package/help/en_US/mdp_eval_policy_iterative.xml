<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="mdp_eval_policy_iterative"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>Page created on July 31, 2001. Last update on November 16,
    2009.</pubdate>
  </info>

  <refnamediv>
    <refname>mdp_eval_policy_iterative</refname>

    <refpurpose>Evaluates a policy using iterations of the Bellman
    operator.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>Vpolicy = mdp_eval_policy_iterative(P, R, discount, policy)
Vpolicy = mdp_eval_policy_iterative(P, R, discount, policy, V0)
Vpolicy = mdp_eval_policy_iterative(P, R, discount, policy, V0, epsilon)
Vpolicy = mdp_eval_policy_iterative(P, R, discount, policy, V0, epsilon, max_iter)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Description</title>

    <para>mdp_eval_policy_iterative evaluates the value fonction associated to
    a policy applying iteratively the Bellman operator.</para>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>P</term>

        <listitem>
          <para>transition probability array.</para>

          <para>P can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>R</term>

        <listitem>
          <para>reward array.</para>

          <para>R can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS) or a 2D array (SxA)
          possibly sparse.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>discount</term>

        <listitem>
          <para>discount factor.</para>

          <para>discount is a real which belongs to [0; 1[.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>policy</term>

        <listitem>
          <para>a policy.</para>

          <para>policy is a (Sx1) vector. Each element is an integer
          corresponding to an action.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>V0 (optional)</term>

        <listitem>
          <para>starting point.</para>

          <para>V0 is a (Sx1) vector representing an inital guess of the value
          function.</para>

          <para>By default, V0 is only composed of 0 elements.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>epsilon (optional)</term>

        <listitem>
          <para>search for an epsilon-optimal policy.</para>

          <para>epsilon is a real greater than 0.</para>

          <para>By default, epsilon = 0.01.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>max_iter (optional)</term>

        <listitem>
          <para>maximum number of iterations.</para>

          <para>max_iter is an integer greater than 0. If the value given in
          argument is greater than a computed bound, a warning informs that
          the computed bound will be used instead.</para>

          <para>By default, max_iter = 1000.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Evaluation</title>

    <variablelist>
      <varlistentry>
        <term>Vpolicy</term>

        <listitem>
          <para>value fonction.</para>

          <para>Vpolicy is a (Sx1) vector.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">-&gt; P = list();
-&gt; P(1) = [ 0.5 0.5;   0.8 0.2 ];
-&gt; P(2) = [ 0 1;   0.1 0.9 ];
-&gt; R = [ 5 10;   -1 2 ];
-&gt; policy = [2;   1];

-&gt; Vpolicy = mdp_eval_policy_iterative(P, R, 0.8, policy)
Vpolicy =
   23.170385
   16.463068

-&gt; mdp_verbose()  // set verbose mode

-&gt; Vpolicy = mdp_eval_policy_iterative(P, R, 0.8, policy)
   Iteration   V_variation
     1      10
     2      6.24
     3      4.992
     4      3.272704
     5      2.6181632
     6      1.7992516
     7      1.4394012
     8      1.0305747
     9      0.8244598
     10      0.6100282
     11      0.4880226
     12      0.3701266
     13      0.2961013
     14      0.2285697
     15      0.1828558
     16      0.1428803
     17      0.1143042
     18      0.0900490
     19      0.0720392
     20      0.0570602
     21      0.0456481
     22      0.0362846
     23      0.0290277
     24      0.0231263
     25      0.0185010
     26      0.0147616
     27      0.0118093
     28      0.0094313
     29      0.0075451
     30      0.0060295
     31      0.0048236
     32      0.0038562
     33      0.0030849
     34      0.0024668
     35      0.0019735
     36      0.0015783
     37      0.0012627
     38      0.0010099
     39      0.0008080
     40      0.0006463
     41      0.0005170
     42      0.0004136
     43      0.0003309
     44      0.0002647
     45      0.0002117
     46      0.0001694
     47      0.0001355
     48      0.0001084
     49      0.0000867
MDP Toolbox: iterations stopped, epsilon-optimal value function
Vpolicy =
   23.170385
   16.463068

In the above example, P can be a list containing sparse matrices:
-&gt; P(1) = sparse([ 0.5 0.5;  0.8 0.2 ]);
-&gt; P(2) = sparse([ 0 1;  0.1 0.9 ]);
The function call is unchanged.</programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Iadine Chadès, Marie-Josée Cros, Frédérick Garcia, Régis
      Sabbadin - INRA</member>
    </simplelist>
  </refsection>
</refentry>
