PRESENTATION
The Markov Decision Processes (MDP) toolbox proposes functions related to 
the resolution of discrete-time Markov Decision Processes : finite horizon,
value iteration, policy iteration, linear programming algorithms 
with some variants.

The toolbox was developped by Iadine Chad�s, Marie-Jos�e Cros, Fr�d�rick Garcia, 
R�gis Sabbadin of the Biometry and Artificial Intelligence Unit of INRA Toulouse
(France). See <http://www.inra.fr/mia/T/MDPtoolbox> for more information.

The third version 3.0 was released on September 21, 2009, under BSD license. 
It takes into account sparse matrices and includes Reinforcement Learning 
based functions.
The fourth version was released in October 31, 2012. It mainly avoid misunderstanding of returned value function that are not the average or the optimal one.

NOTATION
states: set of {1, 2, ..., S}
actions: set of {1, 2, ..., A}
transitions: P(s,s',a) is the probability to reach state s' when the system is 
in state s and action a is performed by the decision maker
rewards: R(s,s',a) is the reward obtained when the system is in state s on decision 
epoch t and is in state s' at decision epoch t+1, with action a performed
R(s,a): reward when the system is in state s at decision epoch t and action a is 
performed by the decision maker


INSTALL
Extract the files from the archive.
Let <PATH> be the path of the directory containing this readme.txt file.
As user execute the following instruction within Scilab:
        exec <PATH>/loader.sce
It can also be put in the user's .scilab startup file for automatic loading.
       
