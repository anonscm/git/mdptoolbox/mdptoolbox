
macros_dir = get_absolute_file_path('buildmacros.sce');

tbx_build_macros(TOOLBOX_NAME, macros_dir);

clear tbx_build_macros;
