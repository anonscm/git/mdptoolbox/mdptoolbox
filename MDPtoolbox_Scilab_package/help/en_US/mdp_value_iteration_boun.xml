<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="mdp_value_iteration_boun"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>Page created on July 31, 2001. Last update on November 16,
    2009.</pubdate>
  </info>

  <refnamediv>
    <refname>mdp_value_iteration_boun</refname>

    <refpurpose>Computes a bound on the number of iterations for the value
    iteration algorithm.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[max_iter, cpu_time] = mdp_value_iteration_boun(P, R, discount)
[max_iter, cpu_time] = mdp_value_iteration_boun(P, R, discount, epsilon)
[max_iter, cpu_time] = mdp_value_iteration_boun(P, R, discount, epsilon, V0)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Description</title>

    <para>mdp_value_iteration_boun computes a bound on the number of
    iterations for the value iteration algorithm to find an epsilon-optimal
    policy with use of span for the stopping criterion.</para>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>P</term>

        <listitem>
          <para>transition probability array.</para>

          <para>P can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>R</term>

        <listitem>
          <para>reward array.</para>

          <para>R can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS) or a 2D array (SxA)
          possibly sparse.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>discount</term>

        <listitem>
          <para>discount factor.</para>

          <para>discount is a real which belongs to [0; 1[.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>epsilon (optional)</term>

        <listitem>
          <para>search for an epsilon-optimal policy.</para>

          <para>epsilon is a real in ]0; 1].</para>

          <para>By default, epsilon = 0.01.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>V0 (optional)</term>

        <listitem>
          <para>starting value function.</para>

          <para>V0 is a (Sx1) vector.</para>

          <para>By default, V0 is only composed of 0 elements.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Evaluation</title>

    <variablelist>
      <varlistentry>
        <term>iter</term>

        <listitem>
          <para>number of done iterations.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cpu_time</term>

        <listitem>
          <para>CPU time used to run the program.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">-&gt; P = list();
-&gt; P(1) = [ 0.5 0.5;   0.8 0.2 ];
-&gt; P(2) = [ 0 1;   0.1 0.9 ];
-&gt; R = [ 5 10;   -1 2 ];

-&gt; max_iter = mdp_value_iteration_boun(P, R,0.9)
max_iter =
   28

In the above example, P can be a list containing sparse matrices:
-&gt; P(1) = sparse([ 0.5 0.5;  0.8 0.2 ]);
-&gt; P(2) = sparse([ 0 1;  0.1 0.9 ]);
The function call is unchanged.</programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Iadine Chadès, Marie-Josée Cros, Frédérick Garcia, Régis
      Sabbadin - INRA</member>
    </simplelist>
  </refsection>
</refentry>
