<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="mdp_policy_iteration_mod"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>Page created on July 31, 2001. Last update on November 16,
    2009.</pubdate>
  </info>

  <refnamediv>
    <refname>mdp_policy_iteration_mod</refname>

    <refpurpose>Solves discounted MDP with modified policy iteration
    algorithm. </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[V, policy, iter, cpu_time] = mdp_value_iteration_mod (P, R, discount)
[V, policy, iter, cpu_time] = mdp_value_iteration_mod (P, R, discount, epsilon)
[V, policy, iter, cpu_time] = mdp_value_iteration_mod (P, R, discount, epsilon, max_iter) </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Description</title>

    <para>mdp_policy_iteration_mod applies the modified policy iteration
    algorithm to solve discounted MDP. The algorithm consists, like policy
    iteration one, in improving the policy iteratively but in policy
    evaluation few iterations (max_iter) of value function updates done.
    </para>

    <para>Iterating is stopped when an epsilon-optimal policy is found.
    </para>

    <para>This function uses verbose and silent modes. In verbose mode, the
    function displays the variation of V for each iteration. </para>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>P</term>

        <listitem>
          <para>transition probability array.</para>

          <para>P can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>R</term>

        <listitem>
          <para>reward array.</para>

          <para>R can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS) or a 2D array (SxA)
          possibly sparse.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>discount</term>

        <listitem>
          <para>discount factor.</para>

          <para>discount is a real which belongs to [0; 1[.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>epsilon (optional)</term>

        <listitem>
          <para>search for an epsilon-optimal policy.</para>

          <para>epsilon is a real in ]0; 1].</para>

          <para>By default, epsilon = 0.01. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>max_iter (optional)</term>

        <listitem>
          <para>maximum number of iterations to be done.</para>

          <para>max_iter is an integer greater than 0.</para>

          <para>By default, max_iter is set to 1000.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Evaluation</title>

    <variablelist>
      <varlistentry>
        <term>V</term>

        <listitem>
          <para>optimal value fonction.</para>

          <para>V is a (Sx1) vector.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>policy</term>

        <listitem>
          <para>optimal policy.</para>

          <para>policy is a (Sx1) vector. Each element is an integer
          corresponding to an action which maximizes the value
          function.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>iter</term>

        <listitem>
          <para>number of iterations.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cpu_time</term>

        <listitem>
          <para>CPU time used to run the program.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">-&gt; P = list();
-&gt; P(1) = [ 0.5 0.5;   0.8 0.2 ];
-&gt; P(2) = [ 0 1;   0.1 0.9 ];
-&gt; R = [ 5 10;   -1 2 ];

-&gt; [V, policy, iter, cpu_time] = mdp_policy_iteration_mod(P, R, 0.9)
cpu_time =
    0.0500
iter =
    5
policy =
    2
    1
V =
    41.865642
    35.47028

-&gt; mdp_verbose()   // set verbose mode

-&gt; [V, policy] = mdp_policy_iteration_mod(P, R, 0.9)
    Iteration   V_variation
          1                 8
          2                 1.6238532
          3                 0.0437728
          4                 0.0011799
          5                 0.0000318
policy =
    2
    1
V =
    41.865642
    35.47028

In the above example, P can be a list containing sparse matrices:
-&gt; P(1) = sparse([ 0.5 0.5;  0.8 0.2 ]);
-&gt; P(2) = sparse([ 0 1;  0.1 0.9 ]);
The function call is unchanged.</programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Iadine Chadès, Marie-Josée Cros, Frédérick Garcia, Régis
      Sabbadin - INRA</member>
    </simplelist>
  </refsection>
</refentry>
