<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="mdp_computePR" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>Page created on July 31, 2001. Last update on November 16,
    2009.</pubdate>
  </info>

  <refnamediv>
    <refname>mdp_computePR</refname>

    <refpurpose>Computes the reward associated to a state/action
    pair.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>PR = mdp_computePR(P, R)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Description</title>

    <para>mdp_computePR computes the reward of a state/action pair, given a
    probability array P and a reward array R possibly depending on arrival
    state.</para>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>P</term>

        <listitem>
          <para>transition probability array.</para>

          <para>P can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>R</term>

        <listitem>
          <para>reward array.</para>

          <para>R can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS) or a 2D array (SxA)
          possibly sparse.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Evaluation</title>

    <variablelist>
      <varlistentry>
        <term>PR</term>

        <listitem>
          <para>reward matrix.</para>

          <para>PR is a (SxA) matrix. </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">-&gt; P = list();
-&gt; P(1) = [0.6116 0.3884;  0 1.0000];
-&gt; P(2) = [0.6674 0.3326;  0 1.0000];
-&gt; R = list();
-&gt; R(1) = [-0.2433 0.7073;  0 0.1871];
-&gt; R(2) = [-0.0069 0.6433;  0 0.2898];

-&gt; PR = mdp_computePR(P, R)
PR =
   0.1259130    0.20943565
   0.1871          0.2898

In the above example, P can be a list containing sparse matrices:
-&gt; P(1) = sparse([0.6116 0.3884;  0 1.0000]);
-&gt; P(2) = sparse([0.6674 0.3326;  0 1.0000]);
The function call is unchanged.</programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Iadine Chadès, Marie-Josée Cros, Frédérick Garcia, Régis
      Sabbadin - INRA</member>
    </simplelist>
  </refsection>
</refentry>
