<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="mdp_finite_horizon"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>Page created on July 31, 2001. Last update on November 16,
    2009.</pubdate>
  </info>

  <refnamediv>
    <refname>mdp_finite_horizon</refname>

    <refpurpose>Solves finite-horizon MDP with backwards induction
    algorithm.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[V, policy, cpu_time] = mdp_finite_horizon (P, R, discount, N)
[V, policy, cpu_time] = mdp_finite_horizon (P, R, discount, N, h)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Description</title>

    <para>mdp_finite_horizon applies backwards induction algorithm for
    finite-horizon MDP. The optimality equations allow to recursively evaluate
    function values starting from the terminal stage.</para>

    <para>This function uses verbose and silent modes. In verbose mode, the
    function displays the current stage and the corresponding optimal
    policy.</para>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>P</term>

        <listitem>
          <para>transition probability array.</para>

          <para>P can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS). </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>R</term>

        <listitem>
          <para>reward array.</para>

          <para>R can be a 3 dimensions array (SxSxA) or a list (1xA), each
          list element containing a sparse matrix (SxS) or a 2D array (SxA)
          possibly sparse. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>discount</term>

        <listitem>
          <para>discount factor.</para>

          <para>discount is a real which belongs to ]0; 1].</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>N</term>

        <listitem>
          <para>number of stages.</para>

          <para>N is an integer greater than 0.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Evaluation</title>

    <variablelist>
      <varlistentry>
        <term>V</term>

        <listitem>
          <para>value fonction.</para>

          <para>V is a (Sx(N+1)) matrix. Each column n is the optimal value
          fonction at stage n, with n = 1, ... N.</para>

          <para>V(:,N+1) is the terminal reward. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>policy</term>

        <listitem>
          <para>optimal policy.</para>

          <para>policy is a (SxN) matrix. Each element is an integer
          corresponding to an action and each column n is the optimal policy
          at stage n.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">-&gt; P = list();
-&gt; P(1) = [ 0.5 0.5;   0.8 0.2 ];
-&gt; P(2) = [ 0 1;   0.1 0.9 ];
-&gt; R = [ 5 10;   -1 2 ];

-&gt; [V, policy, cpu_time] = mdp_finite_horizon(P, R, 0.9, 3)
cpu_time =
   0.0400
policy =
   2 2 2
   1 1 2
V =
   15.9040 11.8000 10.0000 0
     8.6768   6.5600   2.0000 0

-&gt; mdp_verbose()  // set verbose mode

-&gt; [V, policy, cpu_time] = mdp_finite_horizon(P, R, 0.9, 3)
stage:3 policy transpose : 2 2
stage:2 policy transpose : 2 1
stage:1 policy transpose : 2 1
cpu_time =
   0.0400
policy =
   2 2 2
   1 1 2
V =
   15.9040 11.8000 10.0000 0
     8.6768   6.5600   2.0000 0

In the above example, P can be a list containing sparse matrices:
-&gt; P(1) = sparse([ 0.5 0.5;  0.8 0.2 ]);
-&gt; P(2) = sparse([ 0 1;  0.1 0.9 ]);
The function call is unchanged.</programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Iadine Chadès, Marie-Josée Cros, Frédérick Garcia, Régis
      Sabbadin - INRA</member>
    </simplelist>
  </refsection>
</refentry>
