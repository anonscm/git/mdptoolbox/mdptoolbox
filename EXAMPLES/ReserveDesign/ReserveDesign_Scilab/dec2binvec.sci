function out = dec2binvec(dec,n)

//DEC2BINVEC Convert decimal number to a binary vector.
//
//    DEC2BINVEC(D) returns the binary representation of D as a binary
//    vector.  The least significant bit is represented by the first 
//    column.  D must be a non-negative integer. 
// 
//    DEC2BINVEC(D,N) produces a binary representation with at least
//    N bits.
// 
//    Example:
//       dec2binvec(23) returns [1 1 1 0 1]
//
//    See also BINVEC2DEC, DEC2BIN.
//

//    MP 11-11-98
//    Copyright 1998-2003 The MathWorks, Inc.
//    $Revision: 1.1 $  $Date: 2012-12-12 08:37:08 $

// Error if dec is not defined.
if argn(2) < 1
   error('daq:dec2binvec:argcheck', 'D must be defined.  Type ''daqhelp dec2binvec'' for more information.');
end

// Error if D is not a double.
if ~type(dec)==1
   error('daq:dec2binvec:argcheck', 'D must be a double.');
end

// Error if a negative number is passed in.
if (dec < 0)
   error('daq:dec2binvec:argcheck', 'D must be a positive integer.');
end

// Convert the decimal number to a binary string.
if argn(2) == 1
   out1 = dec2bin(dec);
else
   out1 = dec2bin(dec,n);
end

// Convert the binary string, '1011', to a binvec, [1 1 0 1].
//out = evstr([mtlb_fliplr(out);blanks(length(out))]')';
out = [];
out2=strsplit(mtlb_fliplr(out1));
for l=1:1:length(out1)
    out = [out out2(l)];
end
out = bool2s(out=='1');
endfunction
