function state=getState(site)   
    J=length(site);
    baseTern=(3*ones(1,J)).^[0:J-1];
    state=sum(site.*baseTern);
endfunction
