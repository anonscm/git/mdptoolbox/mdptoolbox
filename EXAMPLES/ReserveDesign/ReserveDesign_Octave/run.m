% Add the necessary paths
%addpath('')

% Generate the species richness matrix
rand('seed',0)
M=round(rand(7,20));

% Generate the transition and reward matrix
[P, R] = mdp_example_reserve(M, 0.2);

% Solve the reserve design problem
[policy, iter, cpu_time] = mdp_value_iteration(P, R, 0.96, 0.001);

% Explore solution with initial state all sites available
explore_solution_reserve([0 0 0 0 0 0 0],policy,M,P,R)
